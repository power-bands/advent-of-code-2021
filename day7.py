## Puzzle no.13
## 19'49"
## -> did not read directions carefully enough to supply accurate answer
## -> implementation was straightforward, needed to research `abs` func tho
## -> this can definitely be improved design-wise, lots of waste to think about

import math
pos = None

'''
  - dictionary of all possible alignments
  - loop through positions for each possibility
  -- calculate difference from each position, accumulate total fuel cost
  - position(s) with the least fuel cost are the answer
'''

with open("day7_input.txt") as f:
# with open("scratch.txt") as f:
  pos = [int(x) for x in f.readlines()[0].rstrip("\n").split(",")]

alignments = dict().fromkeys([i for i in range(min(pos), max(pos)+1)], 0)

def fuel_sum(n):
  total = 0
  for i in range(0, n):
    total += i
  return total

for i in range(max(pos)+1):
  for p in pos:
    alignments[i] += sum(range(abs(i - p)+1))


optimal, cost = sorted(alignments.items(), key=lambda x: x[1])[0]
print(optimal, cost)

'''
# cost = 1e20
# optimal = -1
for i,c in alignments.items():
  if c < cost:
    # print(f"{i} costs less than {optimal} ({c} < {cost})")
    cost = c
    optimal = i
  # print(f"{i} no change")
print(optimal, cost)
'''


'''
  - this seems very well suited to a binary search...
  -- either with mean or median
  - well hold up... we don't really have a target value...
  - the actual sorting criteria would yield the answer by default...
'''

# def avg(iter):
#   a = sum(iter)
#   return a // len(iter)


# def median(iter):
#   copy = list(iter)
#   sorted(copy)
#   return copy[len(copy)//2]

# print(avg(pos))
# print(median(pos))

## Puzzle no.14
## 05'17"
## -> simple adjustment, but implementation is still slow
## -> uncommented uses `sorted`