## Puzzle no.15
## 23'41"
## -> description was awfully dense, difficult to parse
## -> working through an example helped, strategy was much simpler than initially thought

'''
  -> split input into displays[display]([signal], [output])
  -> dictionary of segments for each possible signal combo to digit
  -> for each display, create mapping of unique signals by segment count to set of chars
    -> then iterate through output and check for matching digits, accumulating total digits in a master mapping

  -> dictionary of segment IDs to digits per display
    -> first identify patterns for all unique digits
      -> identify segments belonging to 1

  -> mapping of signals to digits, deduced from signals to segments
  -> mapping of segment IDs to characters
  -> mapping of digits to segment IDs

  -> three is the only 5 char signal which contains segments of one
  -> six is the only 6 char signal which is missing one of the segments in one
  -> nine is the only 6 char signal which includes all segments of four
  -> zero is the remaining 6 char signal
  -> six is one segment different than five
  -> two is the remaining 5 character signal

  segment frequency
  -> {9: [3], 4: [5], 6: [6], 8: [1,2], 7: [4,7]}

  -> two is the only signal missing segment 3
  -> segment 1 is the only segment missing from seven
  -> the other 8freq segment is 2
  -> six char signal which contains segments 1,2,3,5,6
    -> remainder is segment 4
  -> last segment is 7 


  -> deduce function
    -> initialize map of signals -> digits
    -> initialize map of segments -> char
    -> identify unique digits (signal len map)

'''

from pprint import pprint
from collections import defaultdict
from collections import Counter

output_digits = dict().fromkeys([i for i in range(10)], 0)
displays = dict()
digits_per_segment_count = {
  2: [1],
  3: [7],
  4: [4],
  5: [2,3,5],
  6: [0,6,9],
  7: [8]
}

seven_segment_frequency = {
  1: 8,
  2: 8,
  3: 9,
  4: 7,
  5: 4,
  6: 6,
  7: 7
}

unique_signal_digit = {
  2: 1,
  3: 7,
  4: 4,
  7: 8
}

digit_to_segment = {
  0: [1,2,3,4,5,6],
  1: [2,3],
  2: [1,2,4,5,7],
  3: [1,2,3,4,7],
  4: [2,3,6,7],
  5: [1,3,4,6,7],
  6: [1,3,4,5,6,7],
  7: [1,2,3],
  8: [1,2,3,4,5,6,7],
  9: [1,2,3,4,6,7]
}

with open("day8_input.txt") as f:
# with open("scratch.txt") as f:
  notes = f.readlines()
  for idx, line in enumerate(notes):
    signal, output = line.rstrip("\n").split(" | ")
    displays[idx] = (signal.split(" "), output.split(" "))


def deduce_output(signal, output):
  segment_to_char = dict().fromkeys([i for i in range(1,8)],"")
  char_frequency = Counter("".join(signal))
  digit_to_signal = dict()
  six_char_signals = []

  for c,v in char_frequency.items(): 
    if v == 4: 
      segment_to_char[5] = c
    if v == 6:
      segment_to_char[6] = c
    if v == 9:
      segment_to_char[3] = c

  for s in signal:
    if len(s) == 2:
      digit_to_signal[1] = s
    elif len(s) == 3:
      digit_to_signal[7] = s
    elif len(s) == 4:
      digit_to_signal[4] = s
    elif len(s) == 7:
      digit_to_signal[8] = s
    elif len(s) == 5:
      # -> #2 is the only signal missing segment 3
      target = set(segment_to_char[3])
      if (target not in set(s)):
        digit_to_signal[2] = s
    elif len(s) == 6:
      six_char_signals.append(s)

  # -> segment 1 is the only segment missing from #7 when compared to #1
  segment_to_char[1] = (set(digit_to_signal[7]) - set(digit_to_signal[1])).pop()
  # -> the other 8freq segment is 2
  segment_to_char[2] = next(filter(lambda item: item[1] == 8 and item[0] != segment_to_char[1], char_frequency.items()))[0]

  # -> six char signal which contains segments 1,2,3,5,6
  known_signal = set("".join(segment_to_char.values()))
  for s in six_char_signals:
    candidate = set(s) - known_signal
    if len(candidate) == 1:
      segment_to_char[4] = candidate.pop()

  known_signal = set("".join(segment_to_char.values()))
  segment_to_char[7] = (set("abcdefg") - known_signal).pop()

  charsets_per_digit = defaultdict(set)

  for i in range(0,10):
    for j in digit_to_segment[i]:
      charsets_per_digit[i].add(segment_to_char[j])

  display_value = 0
  factor = 1000
  for o in output:
    for digit,charset in charsets_per_digit.items():
      if set(o) == charset:
        display_value += digit * factor
        factor //= 10

  # pprint(segment_to_char)
  # pprint(charsets_per_digit)
  print("=> ", display_value)
  return display_value

  # pprint(digit_to_signal)


values = []
for _, d in displays.items():
  values.append(deduce_output(*d))

print(sum(values))

  
  # for o in output:
  #   if len(o) == 2:
  #     output_digits[1] += 1
  #     print(f"-> {o}") 
  #   elif len(o) == 3:
  #     output_digits[7] += 1
  #     print(f"-> {o}") 
  #   elif len(o) == 4:
  #     output_digits[4] += 1
  #     print(f"-> {o}") 
  #   elif len(o) == 7:
  #     output_digits[8] += 1
  #     print(f"-> {o}") 

# pprint(output_digits)
# print(sum(output_digits.values()))

## Puzzle no.16 
## 127'03"
## -> my brain is fried, reasoning, planning, and implementing all took significant time...