## Puzzle no.11
## 24'24"
## -> very easy to reason about, lost time on syntax and logical errors
## -> more careful reading of description, targeted the wrong filename
## -> initial data structure was too complex, good job simplifying

'''
  - loop over days
  - list of fish... the state for initial countdown timer and current...
  - fish are tuples (initial, current)
  - add a new fish when current is zero, using the value of initial + 2
'''

'''
days = 256
cycle = 6
fishes = []
# viz = ""

phi = (1 - math.sqrt(5.0))/2
print(phi)
break

# with open("scratch.txt") as f:
with open("day6_input.txt") as f:
  fishes = list(map(int,f.readlines()[0].rstrip("\n").split(",")))

for d in range(days):
  new_fish = []
  for i, f in enumerate(fishes):
    if f == 0:
      new_fish.append(cycle + 2)
      fishes[i] = cycle
      continue

    fishes[i] -= 1

  fishes.extend(new_fish)
  # viz += f"{d}: {fishes}\n"

print(len(fishes))

# with open("fishes.txt","w") as f:
#   f.writelines(viz)
'''

population = dict().fromkeys([i for i in range(0,9)], 0)

# with open("scratch.txt") as f:
with open("day6_input.txt") as f:
  fishes = list(map(int,f.readlines()[0].rstrip("\n").split(",")))
  for f in fishes:
    population[f] += 1

def growth(initial, days):
  current = initial
  for day in range(0, days):
    due_index = day % 9
    due_count = current[due_index]

    current[(day+7)%9] += due_count
    current[(day+9)%9] += due_count
    current[due_index] = max(0, current[due_index] - due_count)

  return current

# print(sum(growth(population, 80).values()))
print(sum(growth(population, 256).values()))

# print(sum(growth(population, 18).values()))
# print(sum(growth(population, 80).values()))
# print(sum(growth(population, 256).values()))

## Puzzle no.12
## DNF
## -> instead of optimizing data structures, I tried deriving a formula
## -> (very naively, although researched this: https://en.wikipedia.org/wiki/Constant-recursive_sequence)
## -> had to rely on hints from the subreddit, spoiled myself with a solution
## -> heavily adapted https://github.com/plan-x64/advent-of-code-2021/blob/main/advent/day6.py