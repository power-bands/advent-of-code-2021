import fileinput

## Puzzle no.1
## 19"48'
## -> off by one error slowed me down
## -> forgot to omit the first line's diff

'''
prev = -1
diff = 0
increase = 0
decrease = 0

for line in fileinput.input():
  if prev == -1:
    prev = int(line)
    print("skipping first")
    continue

  diff = prev - int(line)

  if diff < 0:
    # print(f"{int(line)} increases over {prev}")
    increase += 1
  else:
    # print(f"{int(line)} is less than or equal to {prev}")
    decrease += 1
  prev = int(line)

print(increase)
print(decrease)
'''

## Puzzle no.2
## 23"23'
## -> carefully checked work by walking through example
## -> avoided off-by-one by double checking work  
## -> still in Q2 territory for a very easy puzzle, but AWARENESS

'''
103
106
107 => 316
110 -> 316 < 323 **
121 -> 323 < 338 **
132 -> 338 < 363 **
147 -> 363 < 400 **
148 -> 400 < 427 **
144 -> 427 < 439 **
141 -> 439 > 433 
147 -> 433 > 432
149 -> 432 < 437 **
'''

prev = 0
window = []
increases = 0

for line in fileinput.input():
  # print(line)
  if len(window) < 3:
    window.append(int(line))
    continue

  if prev == 0: 
    # print(f"window {window}")
    prev = sum(window)

  window.pop(0)
  window.append(int(line))
  # print(f"window {window}")

  if prev < sum(window):
    # print(f"lineno {fileinput.lineno()}: previous window ({prev}) is less than current ({sum(window)})")
    increases += 1

  prev = sum(window)

print(increases)