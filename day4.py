
import re 
from pprint import pprint
## Puzzle no.7
## 57"37'
## -> much more complex iteration, slightly harder to reason about
## -> began writing input processing before reasoning about approach
## -> plan approaches first!!!
## -> minor syntax errors related to type, easily fixed but many errors


with open("day4_input.txt") as f:
# with open("scratch.txt") as f:
  lines = f.readlines()

drawing = [int(no) for no in lines[0].rstrip("\n").split(",")]
boards = dict()

board_key = 1
i = 1
while i < len(lines):
  board = []
  while i < len(lines) and lines[i] != "\n":
    row = [int(no) for no in re.split(r'\D+', lines[i].rstrip("\n").strip(" "))]
    board.append(row)
    i += 1
  if len(board) > 0:
    boards[str(board_key)] = board
    board_key += 1
  i += 1


# Find target numbers in boards, mark them
def mark_number(target, board):
  for row in range(len(board)):
    for col in range(len(board[row])):
      if target == board[row][col]:
        board[row][col] = "X"

def generate_columns(board):
  cols = []

  for col in range(len(board[0])):
    next_column = []
    for row in range(len(board)):
      next_column.append(board[row][col])
    cols.append(next_column)

  return cols

def has_bingo(board):
  combined = [*board]+[*generate_columns(board)]
  for row in range(len(combined)):
    if "".join(map(str,combined[row])) == "XXXXX":
      return True
  return False

def find_winning_board(drawing, boards):
  for no in drawing:
    for key,board in boards.items():
      mark_number(no, board)
      if has_bingo(board):
        return (no,key)

def sum_board(board):
  total = 0
  for row in range(len(board)):
    for col in range(len(board[row])):
      if board[row][col] == "X":
        continue
      else:
        total += int(board[row][col])
  return total

def find_last_winning_board(drawing, boards):
  all_boards = set([key for key in boards.keys()])

  for no in drawing:
    for key, board in boards.items():
      if (key not in all_boards):
        continue

      mark_number(no, board)
      if has_bingo(board):
        if len(all_boards) == 1:
          return (no,key)
        else:
          all_boards.remove(key)


# final, winner = find_winning_board(drawing,boards)
# print(sum_board(boards[winner]) * final)

final, last = find_last_winning_board(drawing,boards)
pprint(boards[last])
print(last, sum_board(boards[last]) * final)

## Puzzle no.8
## 30'39"
## -> not that difficult but kinda dove in, ONCE AGAIN!
## -> sloppy iteration to handle finding last winner
## -> more planning would have helped, my previous design was helpful still