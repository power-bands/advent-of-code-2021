import fileinput
import re

## Puzzle no.3
## 15"38'
## -> forgot to advance loop for forward command
## -> otherwise very straightforward

'''
horizontal = 0 
depth = 0
commands = { "forward": 1, "down": 1, "up": -1 }

for line in fileinput.input():
  tokens = re.split("\W", line)
  
  if tokens[0] == "forward":
    horizontal += int(tokens[1])
    continue

  print(line)
  print("->", horizontal, depth)
  depth += commands[tokens[0]] * int(tokens[1])

print(horizontal, depth, horizontal * depth)
'''

## Puzzle no.4
## 4"52'
## -> tested using the sample input
## -> somewhat confusing description, had to re-read

horizontal = 0 
depth = 0
aim = 0
commands = { "forward": 1, "down": 1, "up": -1 }

for line in fileinput.input():
  tokens = re.split("\W", line)
  
  if tokens[0] == "forward":
    factor = int(tokens[1])
    horizontal += factor
    depth += factor * aim
    continue
  aim += commands[tokens[0]] * int(tokens[1])