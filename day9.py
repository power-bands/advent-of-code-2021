
## Puzzle no.17
## 32'42"
## -> Abuelita Duty

'''

-> convert input into matrix of ints
-> Function for checking the adjacent points of a given coordinate
-> for every position, check to see if it's the lowest
-> if so, store its risk level and return the resulting sum
'''

from pprint import pprint

heightmap = []

with open("day9_input.txt") as f:
# with open("scratch.txt") as f:
  lines = f.readlines()
  for l in lines:
    heightmap.append([int(x) for x in l.rstrip("\n")])

def is_lowest_point(target,heights):

  yA,xA = target
  val = heights[yA][xA]
  d = [(-1, 0),(1, 0),(0, -1),(0, 1)] 
  lower = []

  for dY,dX in d:
    y = yA + dY
    x = xA + dX
    in_bounds_y = y < len(heights) and y > -1
    in_bounds_x = x < len(heights[0]) and x > -1

    if in_bounds_y and in_bounds_x:
      if val < heights[y][x]:
        lower.append(True)
      else:
        lower.append(False)
  
  return all(lower)

risk_level = []

for y in range(len(heightmap)):
  for x in range(len(heightmap[0])):
    val = heightmap[y][x]
    lower = is_lowest_point((y,x), heightmap)
    if lower:
      risk_level.append(val+1)

print(risk_level)
print(sum(risk_level))
