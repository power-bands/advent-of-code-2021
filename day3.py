import fileinput

## Puzzle no.5
## 25'49"
## -> had to learn about bitwise operations and Python binary manipulation
## -> easy to reason about, slow to implement due to reviewing docs

'''
gamma = ""
epsilon = ""
bitlength = 12
columns = [[] for i in range(bitlength)]

for line in fileinput.input():
  bits = line.rstrip("\n")
  # transpose the input into columns
  for i, b in enumerate(bits):
    columns[i].append(int(b))

# sum each column, if sum > len(col)/2 then `1` is the most frequent
for col in columns:
  val = sum(col)
  if val > len(col)/2:
    gamma += "1"
    epsilon += "0"
  else:
    gamma += "0"
    epsilon += "1"

print(gamma)
print(epsilon)
print(int(gamma,2) * int(epsilon,2))
'''

## Puzzle no.6
## 26'10"
## -> trickier iteration than previous problems
## -> very minor type related bugs
## -> spent extra time researching `items()` usage
## -> used the `*` operator to grab the remaining candidates from each set

o2_candidates = set()
# -> criteria is most common values per column, if equal choose 1s
co2_candidates = set()
# -> criteria is least common values per column, if equal choose 0s

def populate_target_bit_map(vals, idx):
  bit_map = {}
  for v in vals:
    bit_map[v] = v[idx]
  return bit_map

# search for O2 Rating
for line in fileinput.input():
  bits = line.rstrip("\n")
  o2_candidates.add(bits)
  co2_candidates.add(bits)

o2_idx = 0
while len(o2_candidates) > 1:
  selection = populate_target_bit_map(o2_candidates, o2_idx)
  # calculate most common value
  most_common = 1 if sum([int(val) for _, val in selection.items()]) >= (len(o2_candidates) / 2) else 0

  # remove disqualified elements
  for k,v in selection.items():
    if int(v) != most_common:
      o2_candidates.remove(k)

  # begin search for next
  o2_idx += 1

print(o2_candidates)

# search for CO2 Rating
co2_idx = 0
while len(co2_candidates) > 1:
  selection = populate_target_bit_map(co2_candidates, co2_idx)
  # calculate most common value
  least_common = 0 if sum([int(val) for _, val in selection.items()]) >= (len(co2_candidates) / 2) else 1

  # remove disqualified elements
  for k,v in selection.items():
    if int(v) != least_common:
      co2_candidates.remove(k)

  # begin search for next
  co2_idx += 1

print(co2_candidates)

print(int(*o2_candidates,2) * int(*co2_candidates,2))