from pprint import pprint 

## Puzzle no.9
## 64"01'
## -> happy with my planning this time, easy to write helpers
## -> prevented premature submission by double-checking results
## -> visualizing data was helpful but still encountered several logical errors

'''

-> build a 2D array to mirror data structure in description
-> parse input as coord pairs, omit those which don't represent horizontal/vertical lines ... 
-> initialize 2D array by tracking max X and max Y values from parsing step
-> iterate through collection of lines, filling array by incrementing each target cell in line
  -> must determine whether line is vertical or horizontal
    -> if x is the same, range over y
    -> if y is the same, range over x
-> count '2's in final grid

'''

max_x = -1
max_y = -1

def min_coord(a,b):
  x1,y1 = a
  x2,y2 = b

  if y1 == y2:
    if x1 - x2 < 1:
      return (a,b)
    elif x2 - x1 < 1:
      return (b,a)

  if x1 == x2:
    if y1 - y2 < 1:
      return (a,b)
    elif y2 - y1 < 1:
      return (b,a)

  # diagonal lines? default, the minimum isn't relevant
  return (a,b)

def line_coords(a, b):
  a, b = min_coord(a,b)
  x1,y1 = a
  x2,y2 = b

  if x1 == x2:
    return list(zip([x1]*(y2-y1), range(y1,y2))) + [b]
  elif y1 == y2:
    return list(zip(range(x1,x2), [y1]*(x2-x1))) + [b]
  else:
    # diagonal lines
    range_x = None
    range_y = None
    if x1 > x2:
      range_x = range(x1,x2,-1)
    else: 
      range_x = range(x1,x2)

    if y1 > y2:
      range_y = range(y1,y2,-1)
    else:
      range_y = range(y1,y2)

    return list(zip(range_x,range_y)) + [b]

def is_straight_line(a, b):
  x1,y1 = a
  x2,y2 = b
  if x1 == x2:
    return True
  elif y1 == y2:
    return True
  else: 
    return False

def search_grid(grid, target):
  total = 0
  for row in range(len(grid)):
    for col in range(len(grid[row])):
      if grid[row][col] >= target:
        total += 1

  return total

def show_grid(grid):
  l = ''
  for row in range(len(grid)):
    for col in range(len(grid[row])):
      if grid[row][col] == 0:
        l += ". "
      else:
        l += f"{grid[row][col]} "
    l += "\n"
  return l


def plot_line(grid, points):
  for p in points:
    x,y = p
    grid[y][x] += 1

  # print("->", points)
  # show_grid(grid)

lines = []

with open("day5_input.txt") as f:
# with open("scratch.txt")as f:
  for l in f.readlines():
    l,r = l.rstrip("\n").split(" -> ")
    a, b = list(map(int, l.split(","))), list(map(int, r.split(",")))
    max_x = max(max_x, a[0], b[0])
    max_y = max(max_y, a[1], b[1])
    lines.append([a,b])
    '''
    if is_straight_line(a, b):
      max_x = max(max_x, a[0], b[0])
      max_y = max(max_y, a[1], b[1])
      lines.append([a,b])
    '''

grid = [[0]*(max_x+1) for _ in range(max_y+1)]

for l in lines:
  c = line_coords(*l)
  plot_line(grid, c)

print(search_grid(grid, 2))
# with open("grid.txt", "w") as f:
#   f.writelines(show_grid(grid))

## Puzzle no.10
## 11"08'
## -> slight adjustment, but not too difficult to reason about
## -> sketched out example in REPL, avoided logical errors